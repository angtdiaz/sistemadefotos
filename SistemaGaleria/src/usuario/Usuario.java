/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

import galeria.Fotografia;

/**
 *
 * @author CltControl
 */
public class Usuario extends Persona {

    private String username, password;

    public Usuario() {
        super("", "", "");
    }

    public Usuario(String usuario, String contrasena, String nombre, String Apellido, String IDE) {
        super(nombre, Apellido, IDE);
        this.username = usuario;
        this.password = contrasena;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setUsername(String usuario) {
        this.username = usuario;
    }

    public void setPassword(String contrasena) {
        this.password = contrasena;
    }

}
