/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

/**
 *
 * @author CltControl
 */
public class Persona {

    private String nombre, apellido, IDE;

    public Persona(String nombre, String Apellido, String IDE) {
        this.nombre = nombre;
        this.apellido = Apellido;
        this.IDE = IDE;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getApellido() {
        return this.apellido;
    }

    public String getIDE() {
        return this.IDE;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String Apellido) {
        this.apellido = Apellido;
    }

    public void setIdentificacion(String identificacion) {
        this.IDE = identificacion;
    }

}
