/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;

//import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import galeria.Album;
import galeria.Fotografia;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import usuario.*;

/**
 *
 * @author Carlos
 */
public class Proceso {

    private static String rutaBaseDatosCliente = "usuario.txt";

    public Proceso() {
        cargarUsuario();
        cargarTodo();
    }

    private static void cargarUsuario() {
        try (BufferedReader bu = new BufferedReader(new FileReader(new File(rutaBaseDatosCliente)));) {
            Usuario user;
            String linea = null;
            while ((linea = bu.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(linea, ", ");
                user = new Usuario();

                user.setUsername(st.nextToken());
                user.setPassword(st.nextToken());
                user.setNombre(st.nextToken());
                user.setApellido(st.nextToken());
                user.setIdentificacion(st.nextToken());

                SistemaGaleria.agregarListUsuarios(user);

            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText(ex.getMessage());
        }
    }

    private static void cargarALbumesPorUsuario() {
        try (ObjectInputStream objLeido = new ObjectInputStream(new FileInputStream("albumesPorUsuario"))) {
            HashMap<String, ArrayList<Album>> albumesPorUsuario = (HashMap<String, ArrayList<Album>>) objLeido.readObject();
            SistemaGaleria.setAlbumesPorUsuario(albumesPorUsuario);
//            System.out.println(SistemaGaleria.getAlbumesPorUsuario());
        } catch (Exception ex) {
            System.out.println("metodoALbumesPorUsuario: " + ex.getMessage());
        }
    }

    private static void cargarFotografiasPorAlbum() {
        try (ObjectInputStream objLeido = new ObjectInputStream(new FileInputStream("fotografiasPorAlbum"))) {
            HashMap<Album, ArrayList<Fotografia>> nuevoMap = new HashMap<>();
            HashMap<Album, ArrayList<Fotografia>> deserializarMap = (HashMap<Album, ArrayList<Fotografia>>) objLeido.readObject();
            Set<String> listUsuarios = SistemaGaleria.getAlbumesPorUsuario().keySet();
            System.out.println(listUsuarios);
            Iterator<String> iterator = listUsuarios.iterator();
            while (iterator.hasNext()) {
                ArrayList<Album> listAlbum = SistemaGaleria.getAlbumesPorUsuario().get(iterator.next());
                for (Album a : listAlbum) {

                }
            }
        } catch (Exception ex) {
            System.out.println("metodoFotografiasPorALbum: " + ex.getMessage());
        }
    }

    private static void cargarTodo() {
        try (ObjectInputStream objLeido = new ObjectInputStream(new FileInputStream("data"))) {
            ArrayList<HashMap> arr = (ArrayList<HashMap>) objLeido.readObject();
            for (int i = 0; i < arr.size(); i++) {
                if (i == 0) {
                    SistemaGaleria.setAlbumesPorUsuario(arr.get(i));
                    System.out.println(SistemaGaleria.getAlbumesPorUsuario());
                } else if (i == 1) {
                    SistemaGaleria.setFotografiasPorAlbum(arr.get(i));
                    System.out.println(SistemaGaleria.getFotografiasPorAlbum());
                }
            }
        } catch (Exception ex) {
            System.out.println("metodoTodo: " + ex.getMessage());
        }
    }
}
