
package sistema;



import galeria.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Carlos
 */
public class SlideShow implements Runnable {

    ImageView imgv = new ImageView();
    Thread t = null;
    private String nomUsuario;
    private boolean validador;
    Album album;
    private int tiempoEspera = 5;
    Slider slider = new Slider();
    BorderPane bp = new BorderPane();

    public SlideShow(String nomUsuario, boolean validador, int tiempoEspera) {
        this.nomUsuario = nomUsuario;
        this.validador = validador;
        this.tiempoEspera = tiempoEspera;
    }

    public void mostrarVentanaSlideShow() {
        slider.setMin(0);
        slider.setMax(15);
        slider.setValue(5);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(1); //cuantas unidades quieres que salga 
        slider.setMinorTickCount(1); //cuantas barritas sale entre numeros
        slider.setBlockIncrement(1); //cuanto se mueve con las direccionales  
        bp.setBottom(slider);

        Stage stageSlideShow = new Stage();
        Pane root = new Pane();
        Button btnIniciarPausar = new Button();
        btnIniciarPausar.setPrefSize(50, 50);
        Button btnDetener = new Button();
        btnDetener.setPrefSize(50, 50);

        Label lblAlbums = new Label("Eliga el album: ");
        lblAlbums.setLayoutX(15);
        lblAlbums.setLayoutY(20);
        ComboBox cmbAlbums = new ComboBox();
        cmbAlbums.setLayoutX(100);
        cmbAlbums.setLayoutY(18);
        root.getChildren().addAll(lblAlbums, cmbAlbums, btnIniciarPausar, btnDetener);
        bp.setTop(root);

        ImageView imgIniciar = new ImageView(new Image(getClass().getResourceAsStream("/imagenes/icon play.jpg")));
        ImageView imgPausar = new ImageView(new Image(getClass().getResourceAsStream("/imagenes/icon pause.png")));
        ImageView imgDetener = new ImageView(new Image(getClass().getResourceAsStream("/imagenes/icon stop.png")));
        imgIniciar.setFitHeight(50);
        imgIniciar.setFitWidth(50);
        imgPausar.setFitHeight(50);
        imgPausar.setFitWidth(50);
        imgDetener.setFitHeight(50);
        imgDetener.setFitWidth(50);
        btnIniciarPausar.setGraphic(imgIniciar);
        btnDetener.setGraphic(imgDetener);
        btnIniciarPausar.setLayoutX(600);
        btnIniciarPausar.setLayoutY(10);
        btnDetener.setLayoutX(680);
        btnDetener.setLayoutY(10);

        cmbAlbums.setOnMouseClicked((e) -> {
            cmbAlbums.getItems().clear();
            for (Album album : SistemaGaleria.getAlbumesPorUsuario().get(nomUsuario)) {
                cmbAlbums.getItems().add(album.getNombreAlbum());
            }
            System.out.println("ss");
        });

        cmbAlbums.setOnMouseDragExited((e) -> {

        });

        imgv.setFitHeight(500);
        imgv.setFitWidth(745);
        bp.setCenter(imgv);

        Scene sceneSlideShow = new Scene(bp, 800, 650);
        stageSlideShow.setTitle("SlideShow");
        stageSlideShow.setScene(sceneSlideShow);
        stageSlideShow.show();

        btnIniciarPausar.setOnMouseClicked((e) -> {
            String nomAlbum = (String) cmbAlbums.getValue();
            for (Album album : SistemaGaleria.getAlbumesPorUsuario().get(nomUsuario)) {
                if (album.getNombreAlbum().equals(nomAlbum)) {
                    System.out.println(nomAlbum);
                    this.album = album;;
                }
            }
            if (album != null) {
                if (btnIniciarPausar.getGraphic().equals(imgIniciar)) {
                    btnIniciarPausar.setGraphic(imgPausar);
                    //                System.out.println();
                    if (t == null) {
                        t = new Thread(this);
                        t.start();
                    } else if (t.getState().toString().equals("TERMINATED")) {
                        validador = true;
                        t = new Thread(this);
                        t.start();
                    } else if (t.getState().toString().equals("TIMED_WAITING")) {
                        t.resume();
                    }
                } else if (btnIniciarPausar.getGraphic().equals(imgPausar)) {
                    btnIniciarPausar.setGraphic(imgIniciar);
                    try {
                        t.suspend();
                        System.out.println(t.getState().toString());

                        //                    Thread.wait();
                    } catch (Exception ex) {

                    }
                }
            }
        });
        btnDetener.setOnMouseClicked((e) -> {
            detener();
            btnIniciarPausar.setGraphic(imgIniciar);
            album = null;
            System.out.println("Proceso Finalizado");
        });

    }

    @Override
    public void run() {
        try {
            int segundos = -1;
            while (validador) {
                ArrayList<Fotografia> listFoto = SistemaGaleria.getFotografiasPorAlbum().get(album);
                for (Fotografia f : listFoto) {
                    Image imgExtraida = f.getImage();
                    imgv.setImage(imgExtraida);
                    tiempoEspera = (int) slider.getValue();
                    Thread.sleep(tiempoEspera * 1000);
                }
            }
        } catch (Exception e) {
            System.out.println("Disculpe, tenemos problemos técnicos");
        }
    }

    public void detener() {
        validador = false;
        System.out.println(this.validador + " " + this.hashCode());
    }
}
