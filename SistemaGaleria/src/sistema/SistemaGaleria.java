/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;

import java.util.ArrayList;
import usuario.*;
import galeria.*;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Set;
import javafx.scene.control.Alert;

/**
 *
 * @author Carlos
 */
public abstract class SistemaGaleria {
    //ATRIBUTOS

    private static HashMap<Album, ArrayList<Fotografia>> fotografiasPorAlbum = new HashMap<>();
    private static HashMap<String, ArrayList<Album>> albumesPorUsuario = new HashMap<>();
    private static ArrayList<Usuario> listUsuarios = new ArrayList();

    //GETTERS
    public static ArrayList<Usuario> getListUsuarios() {
        return listUsuarios;
    }
    //METODOS\

    public static void agregarListUsuarios(Usuario usuario) {
        listUsuarios.add(usuario);
    }

    public static void setFotografiasPorAlbum(HashMap<Album, ArrayList<Fotografia>> mapa) {
        fotografiasPorAlbum = mapa;
        System.out.println(fotografiasPorAlbum);
    }

    public static void setAlbumesPorUsuario(HashMap<String, ArrayList<Album>> mapa) {
        albumesPorUsuario = mapa;
    }

    public static HashMap<Album, ArrayList<Fotografia>> getFotografiasPorAlbum() {
        return fotografiasPorAlbum;
    }

    public static HashMap<String, ArrayList<Album>> getAlbumesPorUsuario() {
        return albumesPorUsuario;
    }

    public static Album getObtenerAlbum(String nomUsuario, String nomAlbum) {
//        ArrayList<Album> alb = new ArrayList();
//        alb.addAll(fotografiasPorAlbum.keySet());
        try {
            for (Album album : albumesPorUsuario.get(nomUsuario)) {
                if (album.getNombreAlbum().equals(nomAlbum)) {
                    return album;
                }
            }
        } catch (Exception ex) {

        }
        return null;
    }

    public static void agregarFotoAUsuarioRegistrado(String nomUsuario, Fotografia fotografia) {
        String nomAlbum = fotografia.getAlbum();
        for (Album album : albumesPorUsuario.get(nomUsuario)) {
            if (album.getNombreAlbum().equals(nomAlbum)) {
                fotografiasPorAlbum.get(album).add(fotografia);
            }
        }
    }

    public static void agregarAlbumAUsuarioRegistrado(String nomUsuario, Album album) {
        albumesPorUsuario.get(nomUsuario).add(album);
    }

    public static void agregarFotoAUsuarioNuevo(String nomUsuario, Fotografia fotografia) {
        ArrayList<Fotografia> arr = new ArrayList();
        arr.add(fotografia);
        String nomAlbum = fotografia.getAlbum();
        for (Album album : albumesPorUsuario.get(nomUsuario)) {
            if (album.getNombreAlbum().equals(nomAlbum)) {
                fotografiasPorAlbum.put(album, arr);
            }
        }
    }

    public static void agregarAlbumAUsuarioNuevo(String nomUsuario, Album album) {
        ArrayList<Album> arr = new ArrayList();
        arr.add(album);
        albumesPorUsuario.put(nomUsuario, arr);
    }

    public static void serializaFotografiasPorAlbum() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("fotografiasPorAlbum"));) {
            oos.writeObject(fotografiasPorAlbum);
            oos.flush();
        } catch (Exception ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(ex.getMessage());
            alert.setContentText(ex.getMessage());
            alert.show();
        }
    }

    public static void serializaAlbumesPorUsuario() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("albumesPorUsuario"));) {
            oos.writeObject(albumesPorUsuario);
            oos.flush();
        } catch (Exception ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(ex.getMessage());
            alert.show();
        }
    }

    public static void serializaTodo() {
        ArrayList<HashMap> prueba = new ArrayList();
        prueba.add(albumesPorUsuario);
        prueba.add(fotografiasPorAlbum);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("data"));) {
            oos.writeObject(prueba);
            oos.flush();
        } catch (Exception ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(ex.getMessage());
            alert.show();
        }
    }

}
