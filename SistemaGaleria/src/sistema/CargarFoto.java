/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;

import galeria.*;
import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author CltControl
 */
public class CargarFoto {

    private Image img;
    Alert alert = new Alert(AlertType.NONE);

    public void entrar(String nomUsuario) {
        Stage primaryStage = new Stage();
        ImageView imgv = new ImageView(new Image(getClass().getResourceAsStream("/imagenes/sinImagen.png")));
        imgv.setFitHeight(250);
        imgv.setFitWidth(400);
        Button btnCargarFoto = new Button("Buscar Foto");
        DatePicker dtpFecha = new DatePicker(LocalDate.now());

        Button btnGuardar = new Button("Guardar");
        Button btnCrearAlbum = new Button("Crear Album");
        TextField txtNombre = new TextField();
        TextField txtDescripcion = new TextField();
        TextField txtLugar = new TextField();
        TextField txtPersonas = new TextField();

        ComboBox cmbAlbums = new ComboBox();

        Pane root = new Pane();
        root.getChildren().addAll(imgv, btnCargarFoto, dtpFecha, cmbAlbums, btnGuardar,
                 txtNombre, txtDescripcion, txtLugar, txtPersonas, btnCrearAlbum);

        imgv.setX(155);
        imgv.setY(20);
        btnCargarFoto.setLayoutX(320);
        btnCargarFoto.setLayoutY(300);

        crearLabels("Nombre: ", 80, 350, root);
        crearLabels("Descripción: ", 80, 390, root);
        crearLabels("Lugar: ", 80, 430, root);
        crearLabels("Personas: ", 80, 470, root);
        crearLabels("Fecha: ", 80, 510, root);
        crearLabels("Album: ", 80, 550, root);

        txtNombre.setLayoutX(150);
        txtNombre.setLayoutY(348);
        txtDescripcion.setLayoutX(150);
        txtDescripcion.setLayoutY(388);
        txtLugar.setLayoutX(150);
        txtLugar.setLayoutY(428);
        txtPersonas.setLayoutX(150);
        txtPersonas.setLayoutY(468);
        txtNombre.setPrefWidth(500);
        txtDescripcion.setPrefWidth(500);
        txtLugar.setPrefWidth(500);
        txtPersonas.setPrefWidth(500);
        dtpFecha.setLayoutX(150);
        dtpFecha.setLayoutY(508);
        cmbAlbums.setLayoutX(150);
        cmbAlbums.setLayoutY(548);
        btnGuardar.setLayoutX(320);
        btnGuardar.setLayoutY(588);
        btnCrearAlbum.setLayoutX(450);
        btnCrearAlbum.setLayoutY(548);
//        txtDescripcion.setPadding(new Insets(10, 10, 10, 10));

        Scene sceneCargarFoto = new Scene(root, 700, 650);
        primaryStage.setTitle("Cargar Foto");
        primaryStage.setScene(sceneCargarFoto);
        primaryStage.show();

        cmbAlbums.setOnMouseClicked((e) -> {
            try {
                cmbAlbums.getItems().clear();
                for (Album album : SistemaGaleria.getAlbumesPorUsuario().get(nomUsuario)) {
                    cmbAlbums.getItems().add(album.getNombreAlbum());
                }
            } catch (Exception ex) {
                alert.setAlertType(AlertType.WARNING);
                alert.setTitle("Alerta");
                alert.setContentText("No se encontraron álbumes creados");
                alert.show();
            }
        });

        btnCrearAlbum.setOnMouseClicked((e) -> {
            new CrearAlbum().entrarCrearAlbum(nomUsuario);
        });
        btnCargarFoto.setOnMouseClicked((e) -> {
            File file = new FileChooser().showOpenDialog(primaryStage);
            try {
                String ruta = file.toURI().toString();
                try {
                    System.out.println(ruta);
                    img = new Image(file.toURI().toString());
                    imgv.setImage(img);
                } catch (Exception ex) {
                    alert = new Alert(AlertType.WARNING);
                    alert.setTitle(ex.getMessage());
                    alert.show();
                }
            } catch (Exception ex) {

            }
        });
        btnGuardar.setOnMouseClicked((e) -> {
            try {
                Fotografia foto = new Fotografia(txtNombre.getText(), txtDescripcion.getText(), txtLugar.getText(), txtPersonas.getText(), dtpFecha.getEditor().getText(), (String) cmbAlbums.getValue());
                foto.setImage(img);
                HashMap<Album, ArrayList<Fotografia>> fotografiasPorAlbum = SistemaGaleria.getFotografiasPorAlbum();
                String nomAlbum = foto.getAlbum();

                for (Album album : SistemaGaleria.getAlbumesPorUsuario().get(nomUsuario)) {
                    if (album.getNombreAlbum().equals(nomAlbum)) {
                        if (fotografiasPorAlbum.containsKey(album)) {
                            SistemaGaleria.agregarFotoAUsuarioRegistrado(nomUsuario, foto);
//                            SistemaGaleria.serializaFotografiasPorAlbum();  
                        } else {
                            SistemaGaleria.agregarFotoAUsuarioNuevo(nomUsuario, foto);
//                            SistemaGaleria.serializaFotografiasPorAlbum();  
                        }
                        SistemaGaleria.serializaTodo();
                    }
                }
                System.out.println(SistemaGaleria.getFotografiasPorAlbum());
                System.out.println(SistemaGaleria.getAlbumesPorUsuario());
                alert.setAlertType(AlertType.INFORMATION);
                alert.setTitle("Foto Guardada Satisfactoriamente!!!");
                primaryStage.close();
            } catch (Exception ex) {
                alert.setAlertType(AlertType.ERROR);
                alert.setTitle(ex.getMessage());
                alert.show();
            }
        });
    }

    public void crearLabels(String nombre, double x, double y, Pane root) {
        Label lbl = new Label(nombre);
        lbl.setLayoutX(x);
        lbl.setLayoutY(y);
        root.getChildren().add(lbl);
    }

    public boolean validaCampos(TextField txtN, ComboBox cmbAlbum) {
        if (txtN.getText().equals("")) {
            alert.setAlertType(AlertType.WARNING);
            alert.setTitle("Ingrese el nombre de la foto");
            alert.show();
            return false;
        } else if (cmbAlbum.getValue().equals("")) {
            alert.setAlertType(AlertType.WARNING);
            alert.setTitle("Ingrese el album a la cual va a pertencer la foto");
            alert.show();
            return false;
        }
        return true;
    }
}
