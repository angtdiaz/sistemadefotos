/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;

import galeria.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Carlos
 */
public class CrearAlbum {

    public void entrarCrearAlbum(String nomUsuario) {
        Stage stageCrearAlbum = new Stage();
        Pane root = new Pane();
        Scene sceneCrearAlbum = new Scene(root, 700, 400);

        TextField txtNombre = new TextField();
        TextField txtDescripcion = new TextField();
        DatePicker dtpFecha = new DatePicker(LocalDate.now());
        Button btnGuardar = new Button("Guardar");

        root.getChildren().addAll(txtNombre, txtDescripcion, dtpFecha, btnGuardar);
        stageCrearAlbum.setScene(sceneCrearAlbum);
        stageCrearAlbum.setTitle("Crear Album");
        stageCrearAlbum.show();

        crearLabels("Nombre: ", 80, 100, root);
        crearLabels("Descripción: ", 80, 140, root);
        crearLabels("Fecha: ", 80, 180, root);
        txtNombre.setLayoutX(150);
        txtNombre.setLayoutY(98);
        txtDescripcion.setLayoutX(150);
        txtDescripcion.setLayoutY(138);
        txtNombre.setPrefWidth(500);
        txtDescripcion.setPrefWidth(500);
        dtpFecha.setLayoutX(150);
        dtpFecha.setLayoutY(178);
        btnGuardar.setLayoutX(330);
        btnGuardar.setLayoutY(275);
        btnGuardar.setScaleX(1.5);
        btnGuardar.setScaleY(1.5);

        btnGuardar.setOnMouseClicked((e) -> {
            guardar(txtNombre, txtDescripcion, dtpFecha, stageCrearAlbum, nomUsuario);
        });

    }

    public void guardar(TextField txtNombre, TextField txtDescripcion, DatePicker dtpFecha, Stage stage, String nomUsuario) {
        Alert alert = new Alert(AlertType.NONE);
        try {
            Album album = new Album(txtNombre.getText(), txtDescripcion.getText(), dtpFecha.getEditor().getText());
            HashMap<String, ArrayList<Album>> albumesPorUsuario = SistemaGaleria.getAlbumesPorUsuario();
            ArrayList<Album> listAlbum = new ArrayList();
            if (albumesPorUsuario.containsKey(nomUsuario)) {
                SistemaGaleria.agregarAlbumAUsuarioRegistrado(nomUsuario, album);
//                    SistemaGaleria.serializaAlbumesPorUsuario();                                     
            } else {
                SistemaGaleria.agregarAlbumAUsuarioNuevo(nomUsuario, album);
//                    SistemaGaleria.serializaAlbumesPorUsuario();
            }
            SistemaGaleria.serializaTodo();
            alert.setAlertType(AlertType.INFORMATION);
            alert.setTitle("Album Creado Satisfactoriamente!!!");
            stage.close();
        } catch (Exception ex) {
            alert.setAlertType(AlertType.ERROR);
            alert.setTitle(ex.getMessage());
        }
    }

    public void crearLabels(String nombre, double x, double y, Pane root) {
        Label lbl = new Label(nombre);
        lbl.setLayoutX(x);
        lbl.setLayoutY(y);
        root.getChildren().add(lbl);
    }
}
