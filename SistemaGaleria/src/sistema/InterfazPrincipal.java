/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;

import galeria.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
//import java.util.Iterator;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author Administrator
 */
public class InterfazPrincipal {

    private static ImageView imageView = new ImageView();
    private static VBox vBoxLeft;
    private static HBox hBox;
    private static TextField tf1, tf2, tf3;
    private Scene scene;
    private static BorderPane bp;
    private static ComboBox albumesTotales, albumesDisponiblesAmover, cmbEliminarAlbum;
    private static GridPane gridPane = new GridPane();
    private static Button todasLasFotos;
    private static Album albumGeneral;
    private static Alert alert = new Alert(AlertType.CONFIRMATION);
    private static RadioButton r1, r2, r3;

    /**
     *
     * @param i Define el espacio entre los nodos de vBox.
     * @return un VBox seteado el espacio entre sus nodos.
     */
    public static VBox createVBox(int i) {
        VBox vBox = new VBox(i);
        return vBox;
    }

    /**
     *
     * @param nomUsuario usado como parametro para otros metodos.
     */
    public static void entrarMenu(String nomUsuario) {
        bp = new BorderPane();
        bp.setStyle("-fx-border-color: #aaaaaa; -fx-background-color: #dddddd;");
        bp.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID,
                CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        hBox = new HBox();
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(20, 20, 20, 20));
        hBox.setStyle("-fx-border-color: black ; -fx-border-width: 1 ; "
                + "-fx-border-style: solid ;"
                + "-fx-background-color:linear-gradient(#7AC1DE, #5A92A9) ;"
                + "-fx-border-insets: 7;");

        hBox.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID,
                CornerRadii.EMPTY, BorderWidths.DEFAULT)));

        vBoxLeft = createVBox(10);
        vBoxLeft.setSpacing(10);
        vBoxLeft.setPadding(new Insets(10, 20, 10, 20));
        vBoxLeft.getStyleClass().add("color-palette");
        vBoxLeft.setStyle("    -fx-border-color: black ; -fx-border-width: 1 ; "
                + "-fx-border-style: solid ;"
                + "-fx-background-color: linear-gradient(#E4EAA2, #9CD672);");
        bp.setTop(hBox);
        bp.setLeft(vBoxLeft);

        Button btnAñadirFoto = new Button("Añadir Foto");
        Button btnAñadirAlbum = new Button("Añadir Album");
        Button btnSlideShow = new Button("SlideShow");
        VBox vb = new VBox();
        ToggleGroup grupo = new ToggleGroup();
        r1 = new RadioButton("Personas");
        r1.setToggleGroup(grupo);
        crearMetodoBusqueda(r1, grupo, nomUsuario);
        r2 = new RadioButton("Lugares");
        r2.setToggleGroup(grupo);
        crearMetodoBusqueda(r2, grupo, nomUsuario);
        r3 = new RadioButton("Fecha");
        r3.setToggleGroup(grupo);
        buscarPorFecha(r3, nomUsuario);
        vb.getChildren().addAll(r1, r2, r3);
        cmbEliminarAlbum = new ComboBox();
        cmbEliminarAlbum.setPromptText("Seleccione el album a eliminar");

        btnAñadirFoto.setPrefSize(150, 20);
        btnAñadirAlbum.setPrefSize(150, 20);
        btnSlideShow.setPrefSize(150, 20);

        cmbEliminarAlbum.setPrefSize(210, 20);

        hBox.getChildren().addAll(btnAñadirFoto, btnAñadirAlbum, btnSlideShow, cmbEliminarAlbum, vb);

        albumesTotales = new ComboBox();
        albumesTotales.setPrefSize(120, 20);
        albumesTotales.setPromptText("Seleccione...");
        todasLasFotos = new Button("Todas las fotos");
        todasLasFotos.setPrefSize(120, 30);
        seleccionarAlbum(nomUsuario);

        vBoxLeft.getChildren().addAll(todasLasFotos, albumesTotales);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane = mostrarTodasLasFotos(nomUsuario);
        gridPane.setHgap(15);
        gridPane.setVgap(10);
        bp.setCenter(gridPane);
        Stage stageGaleria = new Stage();
        Scene sceneGaleria = new Scene(bp, 1220, 650);
        sceneGaleria.setFill(Color.BLACK);

        stageGaleria.setTitle("Galeria");
        stageGaleria.setScene(sceneGaleria);
        stageGaleria.show();

        btnAñadirFoto.setOnMouseClicked((e) -> {
            new CargarFoto().entrar(nomUsuario);
        });
        btnSlideShow.setOnMouseClicked((e) -> {
            SlideShow ss = new SlideShow(nomUsuario, true, 5);
            ss.mostrarVentanaSlideShow();
        });
        todasLasFotos.setOnMouseClicked((e) -> {
            bp.setRight(null);
            bp.setCenter(mostrarTodasLasFotos(nomUsuario));
        });

        eliminarAlbum(nomUsuario);

        btnAñadirAlbum.setOnMouseClicked((e) -> {
            new CrearAlbum().entrarCrearAlbum(nomUsuario);
        });
    }

    /**
     *
     * @param usuario usado para acceder a otros metodos.
     * @param imagen usado para realizar eventos y eliminar imagenes.
     * @param foto usado para obtener sus atributos y modificarlos.
     */
    public static void seleccionarImagen(String usuario, ImageView imagen, Fotografia foto) {
        Button eliminarFotoNew = new Button("Eliminar Foto");
        ComboBox cmbAlbumesDisponiblesAMoverNew = new ComboBox();
        Button editarNew = new Button("Editar");
        Button guardarNew = new Button("Guardar");
        HBox hBoxNew = new HBox();
        hBoxNew.setSpacing(10);

        hBoxNew.getChildren().addAll(eliminarFotoNew, cmbAlbumesDisponiblesAMoverNew, editarNew, guardarNew);
        editarNew.setVisible(true);
        guardarNew.setVisible(false);
        VBox vBoxRightNew = new VBox();
        vBoxRightNew.getStyleClass().add("color-palette");
        vBoxRightNew.setStyle("    -fx-border-color: black ; -fx-border-width: 1 ; "
                + "-fx-border-style: solid ;"
                + "-fx-background-color: linear-gradient(#FD9640, #FE841F);");
        vBoxRightNew.getChildren().addAll(crearLabel("Nombre:"), new Label(foto.getNombre()),
                crearLabel("Descripción::"), new Label(foto.getDescripcion()),
                crearLabel("Nombre de Album al que pertenece:"), new Label(foto.getAlbum()),
                crearLabel("Personas que aparecen: "), new Label(foto.getPersonas()),
                crearLabel("Unicación:"), new Label(foto.getLugar()),
                crearLabel("Fecha:"), new Label(foto.getFecha()),
                hBoxNew);
        imagen.setOnMouseClicked((e) -> {
            if (e.getClickCount() == 1) {
                bp.setRight(vBoxRightNew);
                String nombreDelAlbum = "";
                cmbAlbumesDisponiblesAMoverNew.getItems().clear();
                for (Album ab : listaDeAlbumesPorUsuario(usuario)) {
                    nombreDelAlbum = ab.getNombreAlbum();
                    if (!nombreDelAlbum.equals(foto.getAlbum())) {
                        cmbAlbumesDisponiblesAMoverNew.getItems().add("Mover a: " + nombreDelAlbum);
                    }
                }
                cmbAlbumesDisponiblesAMoverNew.setOnAction((i) -> {
                    System.out.println((String) cmbAlbumesDisponiblesAMoverNew.getValue());

                });
                tf1 = new TextField(foto.getNombre());
                tf2 = new TextField(foto.getDescripcion());
                tf3 = new TextField(foto.getPersonas());
                editarNew.setOnMouseClicked((i) -> {
                    editarNew.setVisible(false);
                    guardarNew.setVisible(true);
                    VBox vb = new VBox();
                    vb.getStyleClass().add("color-palette");
                    vb.setStyle("    -fx-border-color: black ; -fx-border-width: 1 ; "
                            + "-fx-border-style: solid ;"
                            + "-fx-background-color: linear-gradient(#FD9640, #FE841F);");
                    vb.getChildren().addAll(crearLabel("Nombre:"), tf1,
                            crearLabel("Descripción::"), tf2,
                            crearLabel("Nombre de Album al que pertenece:"), new Label(foto.getAlbum()),
                            crearLabel("Personas que aparecen: "), tf3,
                            crearLabel("Ubicación:"), new Label(foto.getLugar()),
                            crearLabel("Fecha:"), new Label(foto.getFecha()),
                            hBoxNew);
                    bp.setRight(vb);
                });
                guardarNew.setOnMouseClicked((a) -> {
                    editarNew.setVisible(true);
                    guardarNew.setVisible(false);
                    vBoxRightNew.getChildren().clear();
                    foto.setNombre(tf1.getText());
                    foto.setDescripcion(tf2.getText());
                    foto.setPersonas(tf3.getText());
                    SistemaGaleria.serializaTodo();
                    vBoxRightNew.getChildren().addAll(crearLabel("Nombre:"), new Label(foto.getNombre()),
                            crearLabel("Descripción::"), new Label(foto.getDescripcion()),
                            crearLabel("Nombre de Album al que pertenece:"), new Label(foto.getAlbum()),
                            crearLabel("Personas que aparecen: "), new Label(foto.getPersonas()),
                            crearLabel("Unicación:"), new Label(foto.getLugar()),
                            crearLabel("Fecha:"), new Label(foto.getFecha()),
                            hBoxNew);
                    bp.setRight(vBoxRightNew);
                });
                eliminarFotoNew.setOnMouseClicked(eliminar -> {
                    mostrarAlerta("ALERTA!", "Está por eliminar esta fotografia", "¿Está seguro de querer eliminarla?", AlertType.CONFIRMATION);
                    if (alert.getResult() == ButtonType.OK) {
                        try {
                            for (Album album : listaDeAlbumesPorUsuario(usuario)) {
                                if (album.getNombreAlbum().equals(foto.getAlbum())) {
                                    listaDeFotografiasPorAlbum(album).remove(foto);
                                    SistemaGaleria.serializaTodo();
                                    gridPane.getChildren().remove(imagen);
                                }
                            }
                        } catch (Exception ex) {
                            System.out.println(ex.getMessage());
                        }
                    }
                });
                cmbAlbumesDisponiblesAMoverNew.setOnAction((mover) -> {
                    String nuevoAlbum = "";
                    for (Album a : listaDeAlbumesPorUsuario(usuario)) {
                        if (a.getNombreAlbum().equals(foto.getAlbum())) {
                            String aux = (String) cmbAlbumesDisponiblesAMoverNew.getValue();
                            nuevoAlbum = aux.substring(9);
                            listaDeFotografiasPorAlbum(a).remove(foto);
                            Album newAlbum = SistemaGaleria.getObtenerAlbum(usuario, nuevoAlbum);
                            System.out.println("foto removida");
                            if (newAlbum != null) {
                                listaDeFotografiasPorAlbum(newAlbum).add(foto);
                                foto.setAlbum(newAlbum.getNombreAlbum());
                                System.out.println("foto agregada");
                            }
                        }
                    }
                    gridPane.getChildren().clear();
                    mostrarTodasLasFotos(usuario);
                    vBoxRightNew.getChildren().clear();

                });

            } else {
                Pane paneFoto = new Pane();
                ImageView fotoAMostrar = new ImageView(foto.getImage());
                fotoAMostrar.setLayoutX(100);

                fotoAMostrar.setFitWidth(600);
                fotoAMostrar.setFitHeight(650);
                paneFoto.getChildren().add(fotoAMostrar);
                Scene sceneFoto = new Scene(paneFoto, 800, 650);
                Stage stageFoto = new Stage();
                stageFoto.setScene(sceneFoto);
                stageFoto.show();
            }
        });
    }

    /**
     *
     * @param album usado en un metodo para obtener un ArrayList de Fotografias
     * usada para llegar un gridPane.
     * @param usuario usada en un metodo para realizar los eventos de mostrar
     * las imagenes en grande y sus datos.
     */
    public static void llenarGridPanePorAlbum(Album album, String usuario) {
        int i;
        int x = 0;
        int y = 0;
        gridPane.getChildren().clear();
        ArrayList<Fotografia> fotos = listaDeFotografiasPorAlbum(album);
        for (i = 0; i < fotos.size(); i++) {
            Fotografia fotografia = fotos.get(i);
            ImageView img = new ImageView();
            img.setImage(fotografia.getImage());
            img.setFitHeight(150);
            img.setFitWidth(150);
            gridPane.add(img, x, y);
            System.out.println(x + " " + y);
            x++;
            if (x % 4 == 0) {
                x = 0;
                y++;
            }
            System.out.println("paso gridpane");
            seleccionarImagen(usuario, img, fotografia);
        }
    }

    /**
     *
     * @param nomUsuario usado para obtener diferentes listas de albumes que
     * dependen del usuario
     */
    public static void seleccionarAlbum(String nomUsuario) {
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        albumesTotales.setOnMouseClicked((e) -> {
            albumesTotales.getItems().clear();

            try {
                ArrayList<Album> albumes = SistemaGaleria.getAlbumesPorUsuario().get(nomUsuario);
                if (albumes.size() > 0) {
                    for (Album al : albumes) {
                        albumesTotales.getItems().add(al.getNombreAlbum());
                    }
                }
            } catch (Exception i) {
                System.out.println("holi, no tienes nada :v");
            }
        });

        albumesTotales.setOnAction((o) -> {
            bp.setRight(null);
            gridPane.getChildren().clear();
            String a = "";
            bp.setCenter(gridPane);
            try {
                for (Album album : listaDeAlbumesPorUsuario(nomUsuario)) {
                    a = (String) albumesTotales.getValue();
                    if (a.equals(album.getNombreAlbum())) {
                        albumGeneral = album;
                        llenarGridPanePorAlbum(albumGeneral, nomUsuario);
                    }
                }
            } catch (Exception e) {
            }

        });

    }

    /**
     *
     * @param usuario usado para abrir una imagen y sus atributos.
     * @return
     */
    public static GridPane mostrarTodasLasFotos(String usuario) {

        gridPane.getChildren().clear();
        try {
            ArrayList<Album> albumes = listaDeAlbumesPorUsuario(usuario);
            int x = 0;
            int y = 0;
            if (albumes != null) {
                for (Album albumNuevo : albumes) {
                    if (SistemaGaleria.getFotografiasPorAlbum().containsKey(albumNuevo)) {
                        for (int i = 0; i < listaDeFotografiasPorAlbum(albumNuevo).size(); i++) {
                            Fotografia fotografia = listaDeFotografiasPorAlbum(albumNuevo).get(i);
                            ImageView img = new ImageView();
                            img.setImage(fotografia.getImage());
                            img.setFitHeight(150);
                            img.setFitWidth(150);
                            gridPane.add(img, x, y);
                            System.out.println(x + " " + y);
                            x++;
                            if (x % 4 == 0) {
                                x = 0;
                                y++;
                            }
                            System.out.println("paso gridpane");
                            seleccionarImagen(usuario, img, fotografia);
                        }
                    }
                }
            } else {
                mostrarAlerta("ATENCION!", "No tiene albums disponibles", "Cree un nuevo album", AlertType.INFORMATION);
            }
        } catch (Exception i) {
            i.printStackTrace();
            System.out.println("tienes un error");

        }

        return gridPane;
    }

    /**
     *
     * @param nomUsuario usado para obtener listas de albumes y para mostrar
     * todas las fotos en el gridPane.
     */
    public static void eliminarAlbum(String nomUsuario) {
        cmbEliminarAlbum.setOnMouseClicked((e) -> {
            try {
                cmbEliminarAlbum.getItems().clear();
                int cont = 0;
                for (Album al : listaDeAlbumesPorUsuario(nomUsuario)) {
                    cmbEliminarAlbum.getItems().add(al.getNombreAlbum());
                    cont++;
                }

            } catch (Exception o) {
                System.out.println("otro error ");
            }
        });

        cmbEliminarAlbum.setOnAction((o) -> {
            try {
                String nomAlbum = (String) cmbEliminarAlbum.getValue();
                if (!nomAlbum.equals("")) {
                    mostrarAlerta("ALERTA!", "La(s) foto(s) del album: " + nomAlbum + ", serán eliminada(s)!!", "¿Está seguro de eliminar este album?", AlertType.CONFIRMATION);
                    if (alert.getResult() == ButtonType.OK) {
//                        String nomAlbum = (String)cmbEliminarAlbum.getValue();
                        Album a = SistemaGaleria.getObtenerAlbum(nomUsuario, nomAlbum);
                        Album albumAEliminar;
                        if (a != null) {
                            listaDeAlbumesPorUsuario(nomUsuario).remove(a);
                            SistemaGaleria.getFotografiasPorAlbum().remove(a);
                            SistemaGaleria.serializaTodo();
                            cmbEliminarAlbum.setPromptText("Seleccione el album a eliminar");
                            mostrarTodasLasFotos(nomUsuario);

                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        });
    }

    /**
     *
     * @param r RadioButton usado para colocar en el gridPane central los datos
     * a qfiltrar segun el tipo de busqueda que se desea.
     * @param grupo ToogleGroup usado para obtener el RadioButton seleccionado.
     * @param nomUsuario String usado para realizar las busquedas por Personas y
     * Lugares.
     */
    public static void crearMetodoBusqueda(RadioButton r, ToggleGroup grupo, String nomUsuario) {
        TextField tx = new TextField();
        tx.alignmentProperty().set(Pos.CENTER);
        Button btnBuscar = new Button("Buscar");
        btnBuscar.setScaleX(1.5);
        btnBuscar.setPadding(new Insets(10, 10, 10, 10));
        r.setOnAction((e) -> {
            bp.setRight(null);
            VBox vb = new VBox();
            vb.getChildren().addAll(tx, btnBuscar);
            vb.setAlignment(Pos.CENTER);
            bp.setCenter(vb);
        });

        btnBuscar.setOnAction((e) -> {
            RadioButton selectedRadioButton = (RadioButton) grupo.getSelectedToggle();
            String valorToggle = selectedRadioButton.getText();
            if (valorToggle.equals("Personas")) {
                filtrarPorPersonas(nomUsuario, tx);
            } else if (valorToggle.equals("Lugares")) {
                filtrarPorLugar(nomUsuario, tx);
            }

        });

    }

    /**
     *
     * @param usuario String usado para obtener una lista de Albumes.
     * @param busqueda TextField usado para obtener el tipo de busqueda(Por
     * personas, lugares, fecha) deseada.
     */
    public static void filtrarPorLugar(String usuario, TextField busqueda) {
        gridPane.getChildren().clear();
        String dato = busqueda.getText().toLowerCase();
        int x = 0, y = 0;
        boolean validacion = false;
        for (Album album : listaDeAlbumesPorUsuario(usuario)) {
            for (Fotografia foto : listaDeFotografiasPorAlbum(album)) {
                if (dato.equals(foto.getLugar().toLowerCase())) {
                    validacion = true;
                    ImageView img = new ImageView();
                    img.setFitHeight(150);
                    img.setFitWidth(150);
                    img.setImage(foto.getImage());
                    gridPane.add(img, x, y);
                    x++;
                    if (x % 4 == 0) {
                        x = 0;
                        y++;
                    }
                    bp.setCenter(gridPane);
                }
            }
        }
        if (validacion == false) {
            mostrarAlerta("ALERTA!", "Lugar no encontrado", "No tiene fotos en este lugar", AlertType.INFORMATION);

        }
    }

    /**
     *
     * @param usuario String usado para obtener una lista de albumes.
     * @param busqueda TextField usado para obtener el tipo de busqueda deseada.
     */
    public static void filtrarPorPersonas(String usuario, TextField busqueda) {
        gridPane.getChildren().clear();
        String dato = busqueda.getText().toLowerCase();
        int x = 0, y = 0;
        boolean validacion = false;
        for (Album album : listaDeAlbumesPorUsuario(usuario)) {
            for (Fotografia foto : listaDeFotografiasPorAlbum(album)) {
                String[] personas = foto.getPersonas().toLowerCase().split(",");
                for (String personaSeleccionada : personas) {
                    if (dato.equals(personaSeleccionada)) {
                        validacion = true;
                        ImageView img = new ImageView();
                        img.setFitHeight(150);
                        img.setFitWidth(150);
                        img.setImage(foto.getImage());
                        gridPane.add(img, x, y);
                        x++;
                        if (x % 4 == 0) {
                            x = 0;
                            y++;
                        }
                        bp.setCenter(gridPane);
                    }
                }
            }
        }
        if (validacion == false) {
            mostrarAlerta("ALERTA!", "Persona no encontrada", "No tiene fotos donde aparezca esta persona", AlertType.INFORMATION);

        }
    }

    /**
     *
     * @param rB radio button utilizado para poder crear el evento de seleccion
     * de fecha
     * @param nomUsuario utilizado para poder acceder a todos los albumes
     */
    public static void buscarPorFecha(RadioButton rB, String nomUsuario) {
        Label lblDesde = crearLabel("Fecha Desde: ");
        DatePicker dtpFeDesde = new DatePicker(LocalDate.now());
        Label lblHasta = crearLabel("Fecha Hasta: ");
        DatePicker dtpFeHasta = new DatePicker(LocalDate.now());

        HBox hbox = new HBox();
        Region spacer = new Region(); // spacer
        spacer.setPrefWidth(40);
        hbox.getChildren().addAll(lblDesde, dtpFeDesde, spacer, lblHasta, dtpFeHasta);
        hbox.setPadding(new Insets(10, 10, 10, 10));
        hbox.setAlignment(Pos.CENTER);
        Button btnBuscar = new Button("Buscar");
        btnBuscar.setScaleX(1.5);
        btnBuscar.setScaleY(1.3);
        rB.setOnAction((e) -> {
            VBox vb = new VBox();
            bp.setRight(null);
            vb.setPadding(new Insets(10, 10, 10, 10));
            vb.getChildren().addAll(hbox, btnBuscar);
            vb.setAlignment(Pos.CENTER);
            bp.setCenter(vb);
            rB.setSelected(false);
        });
        btnBuscar.setOnMouseClicked((e) -> {
            try {
                int x = 0, y = 0;
                gridPane.getChildren().clear();
                for (Album a : listaDeAlbumesPorUsuario(nomUsuario)) {
                    for (Fotografia foto : listaDeFotografiasPorAlbum(a)) {
                        Date start = new SimpleDateFormat("dd/MM/yyyy").parse(dtpFeDesde.getEditor().getText());
                        Date val = new SimpleDateFormat("dd/MM/yyyy").parse(foto.getFecha());
                        Date end = new SimpleDateFormat("dd/MM/yyyy").parse(dtpFeHasta.getEditor().getText());
                        if (val.compareTo(start) >= 0 && end.compareTo(val) >= 0) {
                            System.out.println("funciona1");
                            ImageView img = new ImageView();
                            img.setImage(foto.getImage());
                            img.setFitHeight(150);
                            img.setFitWidth(150);
                            gridPane.add(img, x, y);
                            x++;
                            if (x % 4 == 0) {
                                x = 0;
                                y++;
                            }
                            bp.setCenter(gridPane);
                            seleccionarImagen(nomUsuario, img, foto);
                        }
                    }
                }
            } catch (Exception ex) {
                ex.getStackTrace();
            }
        });
    }

    /**
     *
     * @param a es el titulo de la ventana
     * @param b subtitulo 1 de la ventana
     * @param c subtitulo 2 de la ventana
     * @param tipo es el tipo de alerta que queremos, ya sea de confimacion,
     * advertencia, o mas, para crear nuestra Alerta
     */
    public static void mostrarAlerta(String a, String b, String c, AlertType tipo) {
        alert.setTitle(a);
        alert.setHeaderText(b);
        alert.setAlertType(tipo);
        alert.setContentText(c);
        alert.showAndWait();
    }

    public static ArrayList<Album> listaDeAlbumesPorUsuario(String usuario) {
        ArrayList<Album> lista = SistemaGaleria.getAlbumesPorUsuario().get(usuario);
        return lista;
    }

    public static ArrayList<Fotografia> listaDeFotografiasPorAlbum(Album album) {
        ArrayList<Fotografia> lista1 = SistemaGaleria.getFotografiasPorAlbum().get(album);
        return lista1;
    }

    /**
     *
     * @param texto es el texto que deseamos ponerle a label
     * @return retorna el label con el texto ingresado
     */
    private static Label crearLabel(String texto) {
        Label label = new Label(texto);
        label.setTextFill(Color.web("#872323"));
        label.setFont(Font.font("Cambria", 20));
        label.setStyle("-fx-font-weight: bold");
        return label;
    }

}
