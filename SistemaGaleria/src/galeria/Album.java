/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galeria;

/**
 *
 * @author CltControl
 */
import java.io.Serializable;
import java.util.ArrayList;
import javafx.scene.image.Image;
import usuario.Persona;

public class Album implements Serializable {

    //ATRIBUTOS
    private String nombreAlbum, descripcion, fecha;

    public Album(String nombreAlbum, String descripcion, String fecha) {
        this.nombreAlbum = nombreAlbum;
        this.descripcion = descripcion;
        this.fecha = fecha;
    }

    public String getNombreAlbum() {
        return nombreAlbum;
    }

    public void setNombreAlbum(String nombreAlbum) {
        this.nombreAlbum = nombreAlbum;
    }

    public String getFechaCreacion() {
        return fecha;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fecha = fechaCreacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
