/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import java.util.ArrayList;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import sistema.InterfazPrincipal;
import sistema.Proceso;
import sistema.SistemaGaleria;
import usuario.*;

/**
 *
 * @author Carlos
 */
public class IniciarSesion extends Application {

    Alert alert = new Alert(AlertType.NONE);
    Stage stagePrincipal = new Stage();

    @Override
    public void start(Stage primaryStage) {
        stagePrincipal = primaryStage;
        Proceso proceso = new Proceso();
        Label lblUsuario = new Label();
        lblUsuario.setText("Usuario: ");
        TextField txtUsuario = new TextField();
        lblUsuario.setAlignment(Pos.CENTER);
        Label lblPassword = new Label();
        lblPassword.setText("Contraseña: ");
        TextField txtPassword = new TextField();

        Button btnIniciarSesion = new Button();
        btnIniciarSesion.setText("Iniciar Sesión");

        Pane rootIniciarSesion = new Pane();

        rootIniciarSesion.getChildren().add(lblUsuario);
        rootIniciarSesion.getChildren().add(txtUsuario);
        rootIniciarSesion.getChildren().add(lblPassword);
        rootIniciarSesion.getChildren().add(txtPassword);
        rootIniciarSesion.getChildren().add(btnIniciarSesion);

        lblUsuario.setLayoutX(70);
        lblUsuario.setLayoutY(80);
        lblPassword.setLayoutX(70);
        lblPassword.setLayoutY(120);
        txtUsuario.setLayoutX(160);
        txtUsuario.setLayoutY(80);
        txtPassword.setLayoutX(160);
        txtPassword.setLayoutY(120);
        btnIniciarSesion.setLayoutX(157);
        btnIniciarSesion.setLayoutY(195);
        btnIniciarSesion.setScaleX(2);
        btnIniciarSesion.setScaleY(1.5);

        Scene sceneIniciarSesion = new Scene(rootIniciarSesion, 400, 300);

        primaryStage.setTitle("Sistema Galeria");
        primaryStage.setScene(sceneIniciarSesion);
        primaryStage.show();

        sceneIniciarSesion.setOnKeyPressed((e) -> {
            System.out.println(e.getCode());
            switch (e.getCode()) {
                case ENTER:
                    String user = txtUsuario.getText();
                    String pass = txtPassword.getText();
                    if (validaCampos(txtUsuario, txtPassword)) {
                        ingresarMenu(user, pass);
                    }
                    break;
            }

        });

        btnIniciarSesion.setOnAction((e) -> {
            String user = txtUsuario.getText();
            String pass = txtPassword.getText();
            if (validaCampos(txtUsuario, txtPassword)) {
                ingresarMenu(user, pass);
            }
        });

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public void ingresarMenu(String user, String pass) {
        ArrayList<Usuario> listUsuarios = SistemaGaleria.getListUsuarios();
        String ingreso = "NO";
        for (int i = 0; i < listUsuarios.size(); i++) {
            Usuario usuario = listUsuarios.get(i);
            String u = usuario.getUsername();
            String p = usuario.getPassword();
            if (u.equals(user) && p.equals(pass)) {
                ingreso = "SI";
                stagePrincipal.close();
                InterfazPrincipal.entrarMenu(user);
                alert.setAlertType(AlertType.INFORMATION);
                alert.setContentText("Bienvenido: " + user);
                alert.show();
            }
        }
        if (ingreso.equals("NO")) {
            alert.setAlertType(AlertType.WARNING);
            alert.setContentText("Datos Ingresados Incorrectos");
            alert.show();
        }
    }

    public boolean validaCampos(TextField usuario, TextField pass) {
        if (usuario.getText().equals("") | pass.getText().equals("")) {
            alert.setAlertType(AlertType.WARNING);
            alert.setContentText("Por favor, ingrese usuario/contraseña para iniciar sesión");
            alert.show();
            return false;
        }
        return true;
    }
}
